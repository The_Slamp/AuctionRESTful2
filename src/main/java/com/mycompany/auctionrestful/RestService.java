/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.mycompany.auctionrestful;

import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.Statement;
import java.util.ArrayList;
import java.util.List;
import javax.ws.rs.ApplicationPath;
import javax.ws.rs.GET;
import javax.ws.rs.Path;
import javax.ws.rs.Produces;
import javax.ws.rs.core.Application;
import javax.ws.rs.core.MediaType;
import javax.xml.bind.annotation.XmlRootElement;

/**
 *
 * @author Bruker
 */
@ApplicationPath("/resources")
@Path("/RestServices")
public class RestService extends Application{
    
    @GET
    @Path("/echo")
    public String hello(){
        return"hello";
    }
    
    @GET
    @Path("/Product")
    @Produces(MediaType.APPLICATION_XML)
    public xmlList getActiveProducts() throws ClassNotFoundException{
        String query ="SELECT * FROM Product";
        //PreparedStatement ps = null;
        Statement st = null;
        Connection conn = null;
        ResultSet rs = null;
        List<Product> list = new ArrayList<Product>();
        xmlList xl = new xmlList();
        String url = "jdbc:postgresql://data2.hib.no:5433/db17_g10";
        String user = "g10";
        String password = "8XT6maB*";
        Class.forName("org.postgresql.Driver"); 
        
        try{
            conn = DriverManager.getConnection(url, user, password);
            st = conn.createStatement();
            rs = st.executeQuery(query);
            
            while(rs.next()){
                Product p = new Product();
                p.setPid(rs.getInt("pid"));
                p.setName(rs.getString("name"));
                p.setDescription(rs.getString("description"));
                p.setUid(rs.getInt("uid"));
                p.setImage(rs.getString("image"));
                p.setTimeLeft((Integer)rs.getObject("timeleft"));
                list.add(p);
            }
            xl.setList(list);
           
        }
        catch(Exception e){
            System.out.println("exception");
        }
        return xl;
    }
    
    @GET
    @Path("/ProductJSON")
    @Produces({MediaType.APPLICATION_JSON})
    public Product getActiveProductsJ(){
        return new Product(1,"asd","asd",20);
    }
    
}


@XmlRootElement(name = "xmlList")
class xmlList {

    private List<Product> list;

    public List<Product> getList() {
        return list;
    }

    public void setList(List<Product> list) {
        this.list = list;
    }

}
