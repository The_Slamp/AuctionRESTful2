/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.mycompany.auctionrestful;

import javax.annotation.ManagedBean;
import javax.faces.bean.ApplicationScoped;
import javax.ws.rs.client.Client;
import javax.ws.rs.client.ClientBuilder;

/**
 *
 * @author Bruker
 */
@ManagedBean(value="restClient")
@ApplicationScoped
public class restClient {
    
    
    public void call(){
        Client client = ClientBuilder.newClient();
        
        Product p = client.target("http://localhost:8080/AuctionRESTful/Product").request().get(Product.class);
        
        System.out.println(p);
    }
}
